# Application de Transcription Médicale avec Streamlit et Vertex AI

## Google Disclaimer

Ce produit n'est pas officiellement soutenu par Google.

## Introduction

Cette application a été créée pour démontrer la facilité d'utilisation de l'API Speech-to-Text de Vertex AI pour transcrire des conversations médicales en texte, puis résumer ces transcriptions en utilisant le modèle de génération de texte de Vertex AI. L'application utilise une interface Streamlit pour uploader des fichiers audio, transcrire et résumer le contenu.

![Interface de l'application](image/demo.gif "Application de Transcription Médicale avec Streamlit & Vertex AI")

Pour réaliser cela, nous avons utilisé les technologies suivantes :
- Cloud Run
- Cloud Build
- Artifact Registry
- Vertex AI
- Streamlit Framework
- Python 3.9

## Contenu de ce dépôt

Dans ce dépôt, vous pouvez trouver les fichiers suivants :
```bash
.
├── Dockerfile
├── README.md
├── requirements.txt
├── setup.sh
├── main.py
└── conversation.wav
```

Le **main.py** est le code Python de l'application utilisant l'API Speech-to-Text de Vertex AI et le framework Streamlit.

Le **Dockerfile** permet de containeriser l'application pour faciliter son déploiement sur Cloud Run.

Le **requirements.txt** liste les librairies nécessaires à l'exécution de Python.

Le **setup.sh** est un script Bash à exécuter dans Cloud Shell sur GCP pour configurer l'application de transcription médicale.

## Comment déployer l'application ?

Le déploiement est facilité grâce au script **setup.sh.**

1. Ouvrez CloudShell dans votre console GCP
2. Clonez le dépôt
```bash
git clone https://gitlab.com/jassouline/medical-transcription-using-vertexai-speech-to-text-and-streamlit-on-gcp-cloud-run.git
```
3. Éditez le fichier setup.sh pour configurer les valeurs de votre <PROJECT_ID>, <LOCATION>, etc.
```bash
PROJECT_ID="<VOTRE_PROJECT_ID>"    # Changez cette valeur
LOCATION="us-central1"
...
```
4. Rendez le fichier exécutable
```bash
chmod a+x setup.sh
```
5. Exécutez le script
```bash
./setup.sh
```

## Étapes réalisées par le script setup.sh
Le script **setup.sh** effectue les opérations suivantes pour déployer l'application :

- Vérification et configuration du PROJECT_ID
- Authentification de l'utilisateur de Cloud Shell
- Vérification de l'existence du PROJECT_ID
- Configuration de l'environnement Cloud Shell pour le projet
- Activation des APIs nécessaires (Artifact Registry, Cloud Build, Cloud Run, Vertex AI, Speech-to-Text API)
- Création d'un Artifact Registry pour l'application
- Construction de l'image Docker de l'application avec Cloud Build
- Déploiement de l'image sur Cloud Run
- Configuration de Cloud Run pour accepter les requêtes non authentifiées
- Attribution des rôles IAM nécessaires au compte de service par défaut

À la fin du déploiement, l'URL de l'application sur Cloud Run sera affichée

